# What is a Servlet?
Servlet can be described in many ways, depending on the context.

* Servlet is a technology which is used to create a web application.
* Servlet is an API that provides many interfaces and classes including documentation.
* Servlet is an interface that must be implemented for creating any Servlet.
* Servlet is a class that extends the capabilities of the servers and responds to the incoming requests. It can respond to any requests.
* Servlet is a web component that is deployed on the server to create a dynamic web page.

![](img/response.jpg)

# What is a web application?
A web application is an application accessible from the web. A web application is composed of web components like Servlet, JSP, Filter, etc. and other elements such as HTML, CSS, and JavaScript. The web components typically execute in Web Server and respond to the HTTP request.

# CGI (Common Gateway Interface)
CGI technology enables the web server to call an external program and pass HTTP request information to the external program to process the request. For each request, it starts a new process.

![](img/cgi.jpg)

# Disadvantages of CGI
There are many problems in CGI technology:

1. If the number of clients increases, it takes more time for sending the response.
1. For each request, it starts a process, and the web server is limited to start processes.
1. It uses platform dependent language e.g. C, C++, perl.

# Advantages of Servlet

![](img/response.jpg)

There are many advantages of Servlet over CGI. The web container creates threads for handling the multiple requests to the Servlet. Threads have many benefits over the Processes such as they share a common memory area, lightweight, cost of communication between the threads are low. The advantages of Servlet are as follows:

1. Better performance: because it creates a thread for each request, not process.
1. Portability: because it uses Java language.
1. Robust: JVM manages Servlets, so we don't need to worry about the memory leak, garbage collection, etc.
1. Secure: because it uses java language.

# Run servlet sample locally

You can see an example Java Tomcat based servlet code in this folder.

To run it locally...

```bash
mvn package
sh target/bin/webapp 
```

To run it on a cloud server for free, check out this repository instead:

[heroku/devcenter-embedded-tomcat on Github](https://github.com/heroku/devcenter-embedded-tomcat)

# In order to deploy it...
 to your account you need to create a free Heorku account.

 [Heroku signup page](https://signup.heroku.com/)
